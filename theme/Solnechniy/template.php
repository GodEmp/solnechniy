<?php if(!defined('IN_GS')){ die('you cannot load this page directly.'); }
/****************************************************
*
* @File:         template.php
* @Package:      GetSimple
* @Action:       Solnechniy theme for GetSimple CMS
*
*****************************************************/
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php get_page_clean_title(); ?> / <?php get_site_name(); ?></title>
		<?php get_header(false); ?>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<script src="/theme/Solnechniy/js/jquery.liMenuVert.js"></script>
		<script src="/theme/Solnechniy/js/parallax.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/theme/Solnechniy/style.css" />
	</head>
	<body>
		<div id="container" data-parallax="scroll" data-image-src="/theme/Solnechniy/img/background3.jpg">
			<img src="/theme/Solnechniy/img/logo.png" style="display:block;margin:0 auto;">
			<ul class="menu">
				<?php get_navigation(return_page_slug()); ?>
			</ul>
			<div id="wrapper">
			<?php if (strval(get_page_slug(false)) == 'index') gst_superslideshow('slideshow');	?>
				<div id="sidebar">
					<ul class="sideMenu"><?php get_custom_menu('sidemenu'); ?></ul>
				</div>
				<div id="сontent">
					<h1><?php get_page_clean_title(); ?></h1>
					 <?php get_page_content(); ?>
				</div>
			</div>
		</div>
		<footer>
			<?php get_footer(); ?>
			<div class="social">
				<img src="http://placehold.it/60x80">
				<img src="http://placehold.it/60x80">
				<img src="/theme/Solnechniy/img/vk.png">
			</div>
			© <?php echo date('Y'); ?> <a href="<?php get_site_url(); ?>"> «Солнечный»</a><br>
		</footer>
		<script type="text/javascript">
		$(function(){
		  $('.sideMenu').liMenuVert({
		    delayShow:150,    //Задержка перед появлением выпадающего меню (ms)
		    delayHide:150     //Задержка перед исчезанием выпадающего меню (ms)
		  });
		});
		</script>
	</body>
</html>