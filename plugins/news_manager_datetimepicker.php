<?php
/*

Plugin: News Manager DateTimePicker
Description: Adds date/time picker to News Manager post editor options

Uses: jQuery DateTimePicker - http://xdsoft.net/jqplugins/datetimepicker/
Requires: GetSimple CMS 3.1+, News Manager plugin

*/

define('NMDTPVERSION','0.1.1');

# get correct id for plugin
$thisfile = basename(__FILE__, '.php');

# register plugin
register_plugin(
  $thisfile,
  'News Manager DateTimePicker',
  NMDTPVERSION,
  'Carlos Navarro',
  'http://www.cyberiada.org/cnb/',
  'Date/time picker for News Manager',
  '',
  ''
);

if (basename($_SERVER['PHP_SELF']) == 'load.php' && isset($_GET['id']) && $_GET['id'] == 'news_manager' && isset($_GET['edit'])) {
  register_script('jquery-datetimepicker', $SITEURL.'plugins/news_manager_datetimepicker/js/jquery.datetimepicker.js', '2.3.2', false);
  queue_script('jquery-datetimepicker', GSBACK);
  register_style('jquery-datetimepicker', $SITEURL.'plugins/news_manager_datetimepicker/css/jquery.datetimepicker.css', NMDTPVERSION, 'screen');
  queue_style('jquery-datetimepicker', GSBACK);
  add_action('footer', 'nmdtp_footer_include');
}

function nmdtp_footer_include() {
  global $LANG;
  $lg = substr($LANG, 0, 2);
  if (in_array($lg, array('fi','sk', 'bg','ch','cs','da','de','el','es','fr','hu','it','nl','no','pl','pt','ru','se','sl','tr','vi')))
    $start = 1; // week starts on Monday
  else
    $start = 0; // Sunday
  if (!in_array($lg, array('bg','ch','cs','da','de','el','en','es','fa','fr','hu','it','ja','kr','nl','no','pl','pt','ru','se','sl','th','tr','vi')))
    $lg = 'en'; // fallback to English if not supported by datetimepicker
  if (function_exists('nm_post_title'))
    $format = 'Y-m-d'; // News Manager 2.3+
  else
    $format = 'm/d/Y'; // News Manager 2.2.x-
  // ...
?>
<script type="text/javascript">
  jQuery('#post-date').datetimepicker({
    format:'<?php echo $format; ?>',
    timepicker:false,
    lang:'<?php echo $lg; ?>',
    dayOfWeekStart: <?php echo $start; ?>
  });
  jQuery('#post-time').datetimepicker({
    format:'H:i',
    datepicker:false
  });
</script>
<?php
}
