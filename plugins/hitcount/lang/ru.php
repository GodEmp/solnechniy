<?php
$i18n = array(
    'BROWSER' => "Браузер"
  , 'BROWSER_D' => "Версия"
  , 'DATE_FORMAT_D' => '%d.%m.%Y'
  , 'DATE_FORMAT_W' => 'Недели %V, %Y (%b %#d)'
  , 'DATE_FORMAT_M' => '%b %Y'
  , 'DATE_FORMAT_Q' => 'Квартал, начиная с %b %Y'
  , 'DATE_FORMAT_Y' => '%Y'
  , 'DATE_FORMAT_JQUI' => 'dd.mm.yy'
  , 'DATE_FORMAT_JQPLOT' => '%d.%m.%Y'
  , 'DETAILS' => "Детали"
  , 'DURATION' => "Длительность"
  , 'EXPORT_CSV' => "Экспорт в CSV"
  , 'EXPORT_EXCEL' => "Экспорт в Excel"
  , 'HITS'=> "Запросы"
  , 'HITS_OVER_TIME' => "Запросы по времени"
  , 'SLUG' => "Страница"
  , 'LANG' => "Язык"
  , 'LANG_D' => "Язык и страна"
  , 'OS' => "ОС"
  , 'OS_D' => "Версия ОС"
  , 'REFERER' => "Ссылка"
  ,'COUNTRY' => "Страна"
  , 'REINDEX' => "Повторное индексирование данных"
  , 'RELATIVE' => "относительно"
  , 'SIDEMENU' => "Статистика"
  , 'STACKED' => "сложены"
  , 'TAB_BROWSER' => "Браузеры"
  , 'TAB_DURATION' => "Длительность"
  , 'TAB_SLUG' => "Запросы"
  , 'TAB_LANG' => "Язык"
  , 'TAB_OS' => "ОС"
  , 'TAB_REFERER' => "Ссылки"
  , 'TAB_COUNTRY' => "Страны"
  , 'TITLE_BROWSER' => "Браузеры"
  , 'TITLE_DURATION' => "Длительность"
  , 'TITLE_SLUG' => "Запросы страниц"
  , 'TITLE_LANG' => "Языки"
  , 'TITLE_OS' => "Системы"
  , 'TITLE_REFERER' => "Ссылки"
  , 'TITLE_COUNTRY' => "Страны"
  , 'TO' => "К"
  , 'TOTAL' => "Сумма"
  , 'VISITS' => "Визиты"
  , 'VISITS_OVER_TIME' => "Визитов за время"
  , 'V_DUR_00030' => "0с - 30с"
  , 'V_DUR_00060' => "30с - 1мин"
  , 'V_DUR_00120' => "1мин - 2мин"
  , 'V_DUR_00180' => "2мин - 3мин"
  , 'V_DUR_00240' => "3мин - 4мин"
  , 'V_DUR_00300' => "4мин - 5мин"
  , 'V_DUR_00600' => "5мин - 10мин"
  , 'V_DUR_00900' => "10мин - 15мин"
  , 'V_DUR_01800' => "15мин - 30мин"
  , 'V_DUR_02700' => "30мин - 45мин"
  , 'V_DUR_03600' => "45мин - 1ч"
  , 'V_DUR_99999' => "1ч+"
  , 'V_TOTAL' => "Всего запросов/визитов"
  , 'V_HUMAN' => "Запросов/визитов людей"
  , 'V_ROBOT' => "Запросов/визитов роботов"
  , 'REFRESH' => "Обновить"
  , 'DEL_COOKIE' => "Отключить черный список"
  , 'DOWNLOAD_IP2COUNTRY' => "Скачать базу данных IP страны"
  , 'SET_COOKIE' => "Включить черный список"
  , 'SHOW_WORLDMAP' => "Показать карту мира"
);